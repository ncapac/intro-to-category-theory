\contentsline {section}{\tocsection {}{1}{Instructions}}{1}{section.1}
\contentsline {subsection}{\tocsubsection {}{1.1}{}}{1}{subsection.1.1}
\contentsline {subsection}{\tocsubsection {}{1.2}{}}{2}{subsection.1.2}
\contentsline {section}{\tocsection {}{2}{Week 1}}{2}{section.2}
\contentsline {subsection}{\tocsubsection {}{2.1}{}}{2}{subsection.2.1}
\contentsline {subsection}{\tocsubsection {}{2.2}{}}{3}{subsection.2.2}
\contentsline {subsection}{\tocsubsection {}{2.3}{}}{3}{subsection.2.3}
\contentsline {subsection}{\tocsubsection {}{2.4}{}}{4}{subsection.2.4}
\contentsline {subsection}{\tocsubsection {}{2.5}{}}{4}{subsection.2.5}
\contentsline {subsection}{\tocsubsection {}{2.6}{}}{4}{subsection.2.6}
\contentsline {subsection}{\tocsubsection {}{2.7}{}}{4}{subsection.2.7}
\contentsline {subsection}{\tocsubsection {}{2.8}{}}{4}{subsection.2.8}
\contentsline {subsection}{\tocsubsection {}{2.9}{}}{5}{subsection.2.9}
\contentsline {subsection}{\tocsubsection {}{2.10}{}}{5}{subsection.2.10}
\contentsline {subsection}{\tocsubsection {}{2.11}{}}{6}{subsection.2.11}
\contentsline {section}{\tocsection {}{3}{Discussion: Week 1}}{6}{section.3}
\contentsline {section}{\tocsection {}{4}{Week 2}}{7}{section.4}
\contentsline {subsection}{\tocsubsection {}{4.1}{}}{7}{subsection.4.1}
\contentsline {subsection}{\tocsubsection {}{4.2}{}}{7}{subsection.4.2}
\contentsline {subsection}{\tocsubsection {}{4.3}{}}{7}{subsection.4.3}
\contentsline {subsection}{\tocsubsection {}{4.4}{}}{9}{subsection.4.4}
\contentsline {subsection}{\tocsubsection {}{4.5}{}}{9}{subsection.4.5}
\contentsline {subsection}{\tocsubsection {}{4.6}{}}{9}{subsection.4.6}
\contentsline {subsection}{\tocsubsection {}{4.7}{}}{9}{subsection.4.7}
\contentsline {subsection}{\tocsubsection {}{4.8}{}}{10}{subsection.4.8}
\contentsline {subsection}{\tocsubsection {}{4.9}{}}{10}{subsection.4.9}
\contentsline {subsection}{\tocsubsection {}{4.10}{}}{10}{subsection.4.10}
\contentsline {subsection}{\tocsubsection {}{4.11}{}}{11}{subsection.4.11}
\contentsline {subsection}{\tocsubsection {}{4.12}{}}{11}{subsection.4.12}
\contentsline {subsection}{\tocsubsection {}{4.13}{}}{11}{subsection.4.13}
\contentsline {subsection}{\tocsubsection {}{4.14}{}}{11}{subsection.4.14}
\contentsline {section}{\tocsection {}{5}{Discussion: Week 2}}{12}{section.5}
\contentsline {section}{\tocsection {}{6}{Week 3}}{13}{section.6}
\contentsline {subsection}{\tocsubsection {}{6.1}{}}{13}{subsection.6.1}
\contentsline {subsection}{\tocsubsection {}{6.2}{}}{15}{subsection.6.2}
\contentsline {subsection}{\tocsubsection {}{6.3}{}}{15}{subsection.6.3}
\contentsline {subsection}{\tocsubsection {}{6.4}{}}{16}{subsection.6.4}
\contentsline {subsection}{\tocsubsection {}{6.5}{}}{16}{subsection.6.5}
\contentsline {subsection}{\tocsubsection {}{6.6}{}}{17}{subsection.6.6}
\contentsline {subsection}{\tocsubsection {}{6.7}{}}{17}{subsection.6.7}
\contentsline {subsection}{\tocsubsection {}{6.8}{}}{17}{subsection.6.8}
\contentsline {subsection}{\tocsubsection {}{6.9}{}}{17}{subsection.6.9}
\contentsline {section}{\tocsection {}{7}{Week 4}}{17}{section.7}
\contentsline {subsection}{\tocsubsection {}{7.1}{}}{17}{subsection.7.1}
\contentsline {subsection}{\tocsubsection {}{7.2}{}}{18}{subsection.7.2}
\contentsline {subsection}{\tocsubsection {}{7.3}{}}{19}{subsection.7.3}
\contentsline {subsection}{\tocsubsection {}{7.4}{}}{20}{subsection.7.4}
\contentsline {subsection}{\tocsubsection {}{7.5}{}}{20}{subsection.7.5}
\contentsline {subsection}{\tocsubsection {}{}{Pull-back Functor}}{20}{section*.2}
\contentsline {subsection}{\tocsubsection {}{7.6}{}}{21}{subsection.7.6}
\contentsline {subsection}{\tocsubsection {}{7.7}{}}{21}{subsection.7.7}
\contentsline {section}{\tocsection {}{8}{Week 5}}{21}{section.8}
\contentsline {subsection}{\tocsubsection {}{8.1}{}}{21}{subsection.8.1}
\contentsline {subsection}{\tocsubsection {}{8.2}{}}{22}{subsection.8.2}
\contentsline {subsection}{\tocsubsection {}{8.3}{}}{22}{subsection.8.3}
\contentsline {subsection}{\tocsubsection {}{8.4}{}}{22}{subsection.8.4}
\contentsline {subsection}{\tocsubsection {}{8.5}{}}{23}{subsection.8.5}
\contentsline {subsection}{\tocsubsection {}{8.6}{}}{23}{subsection.8.6}
\contentsline {section}{\tocsection {}{9}{Discussion: Week 5}}{23}{section.9}
\contentsline {section}{\tocsection {}{10}{Week 6}}{25}{section.10}
\contentsline {subsection}{\tocsubsection {}{10.1}{}}{25}{subsection.10.1}
\contentsline {subsection}{\tocsubsection {}{10.2}{}}{26}{subsection.10.2}
\contentsline {subsection}{\tocsubsection {}{10.3}{}}{26}{subsection.10.3}
\contentsline {subsection}{\tocsubsection {}{10.4}{}}{27}{subsection.10.4}
\contentsline {subsection}{\tocsubsection {}{10.5}{}}{27}{subsection.10.5}
\contentsline {subsection}{\tocsubsection {}{10.6}{}}{27}{subsection.10.6}
\contentsline {subsection}{\tocsubsection {}{10.7}{}}{29}{subsection.10.7}
\contentsline {subsection}{\tocsubsection {}{10.8}{}}{30}{subsection.10.8}
\contentsline {subsection}{\tocsubsection {}{10.9}{}}{30}{subsection.10.9}
\contentsline {section}{\tocsection {}{11}{The Yoneda Lemma}}{30}{section.11}
\contentsline {section}{\tocsection {}{12}{Week 7}}{35}{section.12}
\contentsline {subsection}{\tocsubsection {}{12.1}{}}{35}{subsection.12.1}
\contentsline {subsection}{\tocsubsection {}{12.2}{}}{35}{subsection.12.2}
\contentsline {subsection}{\tocsubsection {}{12.3}{}}{35}{subsection.12.3}
\contentsline {subsection}{\tocsubsection {}{12.4}{}}{36}{subsection.12.4}
\contentsline {subsection}{\tocsubsection {}{12.5}{}}{36}{subsection.12.5}
\contentsline {subsection}{\tocsubsection {}{12.6}{}}{37}{subsection.12.6}
\contentsline {subsection}{\tocsubsection {}{12.7}{}}{37}{subsection.12.7}
\contentsline {subsection}{\tocsubsection {}{12.8}{}}{37}{subsection.12.8}
\contentsline {subsection}{\tocsubsection {}{12.9}{}}{38}{subsection.12.9}
\contentsline {section}{\tocsection {}{13}{Week 8}}{38}{section.13}
\contentsline {section}{\tocsection {}{14}{Week 9}}{39}{section.14}
\contentsline {subsection}{\tocsubsection {}{14.1}{}}{39}{subsection.14.1}
\contentsline {subsection}{\tocsubsection {}{14.2}{}}{41}{subsection.14.2}
\contentsline {subsection}{\tocsubsection {}{14.3}{}}{42}{subsection.14.3}
\contentsline {subsection}{\tocsubsection {}{14.4}{}}{42}{subsection.14.4}
\contentsline {subsection}{\tocsubsection {}{14.5}{}}{42}{subsection.14.5}
\contentsline {subsection}{\tocsubsection {}{14.6}{}}{43}{subsection.14.6}
\contentsline {subsection}{\tocsubsection {}{14.7}{}}{43}{subsection.14.7}
\contentsline {subsection}{\tocsubsection {}{14.8}{}}{43}{subsection.14.8}
\contentsline {section}{\tocsection {}{15}{Week 10}}{43}{section.15}
\contentsline {subsection}{\tocsubsection {}{15.1}{}}{43}{subsection.15.1}
\contentsline {subsection}{\tocsubsection {}{15.2}{}}{44}{subsection.15.2}
\contentsline {subsection}{\tocsubsection {}{15.3}{}}{45}{subsection.15.3}
\contentsline {subsection}{\tocsubsection {}{15.4}{}}{45}{subsection.15.4}
\contentsline {subsection}{\tocsubsection {}{15.5}{}}{45}{subsection.15.5}
\contentsline {subsection}{\tocsubsection {}{15.6}{}}{46}{subsection.15.6}
\contentsline {subsection}{\tocsubsection {}{15.7}{}}{46}{subsection.15.7}
\contentsline {subsection}{\tocsubsection {}{15.8}{}}{46}{subsection.15.8}
